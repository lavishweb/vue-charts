import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function load(component) {
    // '@' is aliased to src/components
    return () =>
        import (`@/${component}.vue`)
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max))
}

var router

export default router = new VueRouter({
    /*
     * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
     * it is only to be used only for websites.
     *
     * If you decide to go with "history" mode, please also open /config/index.js
     * and set "build.publicPath" to something other than an empty string.
     * Example: '/' instead of current ''
     *
     * If switching back to default "hash" mode, don't forget to set the
     * build publicPath back to '' so Cordova builds work again.
     */

    // mode: 'history',
    scrollBehavior: () => ({ y: 0 }),

    routes: [{
            path: '/web/:dashboard',
            component: load('WebDashboard'),
            children: [{
                // UserProfile will be rendered inside User's <router-view>
                // when /user/:id/profile is matched
                path: ':identifier',
                component: load('WebDashboard')
            }],
            props: (route => ({
                key: getRandomInt(100000000)
            }))
        },
        {
            path: '/reports/:dashboard',
            component: load('WebDashboard'),
            children: [{
                path: ':identifier',
                component: load('WebDashboard')
            }]
        },
        {
            path: '/search/:dashboard',
            component: load('DataSearch')
        },
        { path: '/iframe', component: load('IframeTest') },
        { path: '/dashboard_creator', component: load('MCreator') },
        { path: '*', component: load('Error404') } // Not found
    ]
})

router.beforeEach((to, from, next) => {
    if (window.rootApp) {
        window.rootApp.changeKey()
    }
    history.pushState(null, '')
    next()
})