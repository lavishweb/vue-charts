# VueCharts is an open-source BI Tool and Analytics Platform that runs on Vue and eCharts.

## It allows you to build various eCharts graphs on dashboards using metadata stored in backend configuration routes.

### This repo is still in progress. Most of the frontend features work, but need polish.

### This framework is designed to be plugged into any backend that supports storing json(Redis, MySQL, etc.)

0. clone the repo `$ git clone https://github.com/syntacticsolutions/VueCharts.git`

0. Install dependencies `$ npm install`

0. Run with webpack and hot module swapping `$ npm run dev`

0. Build for production `$npm run build`

## Some of the documentation will be ready by January 14th
