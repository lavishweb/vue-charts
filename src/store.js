import Vue from 'vue'
import Vuex from 'vuex'

var _ = {
    each: require('lodash/each')
}

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        json: [],
        filters: {}
    },
    mutations: {
        clearData (state) {
            while (state.json.length) {
                state.json.pop()
            }
        },
        setData (state, json) {
            Vue.set(state, 'json', json)
        },
        appendData (state, json) {
            state.json = [...state.json, ...json]
        },
        setFilter (state, [cat, arr]) {
            Vue.set(state.filters, cat, arr)
        },
        setFilters (state, filters) {
            let storeFilters = {}
            filters.forEach(filter => {
                storeFilters[filter.yVal] = filter.default
            })
            Vue.set(state, 'filters', storeFilters)
        },
        deleteFilter (state, [filter, chip]) {
            Vue.delete(state.filters[filter], state.filters[filter].indexOf(chip))
        },
        clearFilters (state) {
            _.each(state.filters, (prop, key) => {
                Vue.delete(state.filters, key)
            })
        }
    }
})