// stuff that should always be refreshed no matter what
const dynamic = [
    'hot-update.json', //for webpack development
	'__webpack_hmr',  // for webpack development
	'sw.js', // always refresh the serviceworker
	'chrome-extension' // for vue-devtools
]

// stuff the never changes
const static = [
	'https://unpkg.com/ionicons'
]

// same as dynamic except with absolute paths
const absolutes = [
	'https://absolute/path/to/external/scripts',
]

const containsDynamicContent = str => dynamic.filter((el, ind) => str.toLowerCase().indexOf(el) > -1 || str === '/').length

const containsStaticContent = str => static.filter((el, ind) => str.toLowerCase().indexOf(el) > -1).length

self.addEventListener('install', (event) => {
    console.log('Marauders map web worker installed.')
});
  
self.addEventListener('fetch', function(event) {

	if (containsDynamicContent(event.request.url) || absolutes.indexOf(event.request.url) > -1 || (event.request.method !== 'GET' && event.request.method !== 'OPTIONS')) {
		return
	}

    try {
        let cache
  
        event.respondWith(
            caches
              .open('dynamic-content-v1')
              .then(function(cache) {
				  cache = cache
				  if (containsDynamicContent(event.request.url)) {
					  return
				  }
                  else {
                      return cache
                          .match(event.request)
                          .then(response => {
							  console.log('Exists in cache: ' + event.request.url)
							  let resp = response ? response.clone() : ''
							  if (resp) {
								  if(resp.headers.has('Last-Modified')) {
										let lastModified = resp.headers.get('Last-Modified'),
										date = new Date().getTime()
										let timeSinceUpdate = date - new Date(lastModified).getTime()
										if (timeSinceUpdate > (18000 * 100) && !containsStaticContent(event.request.url) || !resp.body || resp.status !== 200) {
											return cache.delete(event.request)
												.then(success => {
													console.log('5 hour old endpoint cache renewed', event.response)
													if (success) {
														return fetch(event.request).then(res => {
															if (res.status === 200) cache.put(event.request, res.clone())
															return res
														})
													}
													else {
														return response
													}
												})
										}
								  }
								  else {
									  return cache.delete(event.request)
									  	.then(success => {
											if (success) {
												return fetch(event.request).then(res => {
													cache.put(event.request, res.clone())
													return res
												})
											}
										})
								  }
							  }
                              return response || fetch(event.request).then(finalResponse => {
								  if (event.request.method === 'GET') cache.put(event.request, finalResponse.clone())
                                  return finalResponse
                              })
                          })
                  }
              })
        )
    }
    catch (err) {
        console.log(err)
    }
              // console.log(cache)
              // check if the requested URL is inside the dynamic-content-v1
  })

// this is how we access all of the registered serviceworkers in order to do maintenence on them
// unregistring, and debugging

// navigator.serviceWorker.getRegistrations().then(
//   function(registrations) {
//       for(let registration of registrations) {  
//           registration.unregister();
//       }
// });