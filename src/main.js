'use strict'
import Vue from 'vue'
import router from './router'
import store from './store'
import './vue-charts.config'
import axios from 'axios'

Vue.prototype.$axios = axios

window.store = store

Vue.config.productionTip = false

new Vue({
    el: '#q-app',
    data: {
        config: [],
        data: []
    },
    router,
    store,
    render: h => h(require('./App').default)
})
